DailyMotionUpload
=================

Bash script to upload videos to DailyMotion from command line, very simple to configure.

Just add the client_id, client_secret, username, password and scope details to configure the script. change the path of the video file to match it on your system and run it from a Bash shell.

###Usage
```bash 
	./dmUpload.sh